// Create documents to use

db.fruits.insertMany([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplierId: 1,
		onSale: true,
		origin: ["Philippines", "US"]
	},
	{
		name: "Banana",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplierId: 2,
		onSale: true,
		origin: ["Philippines", "Ecuador"]
	},
	{
		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplierId: 1,
		onSale: true,
		origin: ["US", "China"]
	},
	{
		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplierId: 2,
		onSale: false,
		origin: ["Philippines", "India"]
	}
]);

// [SECTION] MongoDB Aggregation
/*
	- Used to generate manipulated data and perform operations to create filtered results that hep in analyzing data
*/
// Using the aggregate method
/*
	- The $match method is used to pass the documents that meet the specified conditions/s to the next pipeline stage/aggregation process
	Syntax:
	{$match: {field: value}}

	- The $group is used to group elements together and field-value pairs using the data from the grouped elements
	Syntax:
	{$group: {_id: "value", fieldResult: "valueResult" }}

	- Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group the total amount of stocks for all suppliers found.
	- Syntax
	  - db.collectionName.aggregate([
	    { $match: { fieldA, valueA } },
	    { $group: { _id: "$fieldB" }, { result: { operation } } }
	  ])
	- The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
	- The "$sum" operator will total the values of all "stock" fields in the returned documents that are found using the "$match" criteria.
*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", total: {$sum: "$stock"}}}
]);


// Field projection with aggregation
/*
	- $project can be used when aggregating data to include/ exclude fields from the returned results
	Syntax:
	{ $project: {field: 1/0}}
*/
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", total: {$sum: "$stock"}}},
	{$project: { _id: 0 }}
]);

// Sorting aggragted results
/*
	$sort can be usde to change the order of aggragated results
	-Providing a value of 1 will sort the aggragated result in asending order/ -1 descending
	Syntax:
	{$sort {field: 1/-1}}
*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", total: {$sum: "$stock"}}},
	{$sort: { total: 1}}
]);

// Aggregating result based on array fields
/*
	$unwind deconstructs an array field from a collection/ field with an array value to output a result for each element
	Syntax:
	{$unwind: field}
*/
db.fruits.aggregate([
	{$unwind: "$origin"}
]);

// Displays fruits documents by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id: "$origin", fruits: {$sum: 1}}}
]);

// [SECTION] Other Aggregate Stages

// $count all yellow fruits

db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$count: "Yellow Fruits"}
]);

// $avg gets the average value of the stock

db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", yellowFruitsStock: {$avg: "$stock"}}}
]);

// $min & $max

db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", yellowFruitsStock: {$min: "$stock"}}}
]);

db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", yellowFruitsStock: {$max: "$stock"}}}
]);